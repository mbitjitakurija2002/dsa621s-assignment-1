import ballerina/http;

Student [] students=[];

listener http:Listener ep0 = new (8080, config = {host: "localhost"});

type Student record{|
    readonly decimal studentnumber;
    string name;
    string emailaddress;  
    string coursecode;
    decimal marks;
    |};
    
table<Student> key(studentnumber) student = table [
        {studentnumber: 221047220, name: "Junias Tumbila", emailaddress: "juniasalonsot22@gmail.com", coursecode:"WAD621S",marks :45},
        {studentnumber: 221047210, name: "Mbitjita Kurija", emailaddress: "mbitjita45@gmail.com",coursecode:"WAD621S",marks :55},
        {studentnumber: 221047211, name: "Paolo Mbahuma", emailaddress: "credo@gmail.com",coursecode:"WAD621S",marks :67},
        {studentnumber: 221043763, name: "Anoeshka Bernadete Iipinge", emailaddress: "deteiipinge@gmail.com",coursecode:"WAD621S",marks :90}
    ];

service / on ep0 {
    resource function get students() returns Student[]{
        return students;
    }
    resource function post students (@http:Payload Student payload) returns  Student {
     students.push(payload);
     return payload;
    }

    resource function get students/[decimal studentnumber]() returns Student|http:NotFound {
        Student? students = student[studentnumber];
        if students is () {
            return http:NOT_FOUND;
        } else {
            return students;
        }
    }

    resource function delete students/[decimal studentnumber](@http:Payload Student students) returns Student|http:NotFound{
        Student? students1  = student[studentnumber];
        if students1 is (){
            return http:NOT_FOUND;
        }else{
            Student remove = student.remove(studentnumber);
            return remove;
        }
    }    
 }
